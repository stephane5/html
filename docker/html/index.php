<?php
// Generate valid URL parameter
$secret = "cloudflare";
$time   = time();
$param  = $time . "-" . urlencode(base64_encode(hash_hmac("sha256", "/tokenauth/kayak.mp4$time", $secret, true)));
$url    = "/tokenauth/kayak.mp4?verify=" . $param;

?>

    <!DOCTYPE html>
    <link rel="amphtml" href="https://www.justalittlebyte.ovh/amp.html">
    <link rel="stylesheet" href="main.css">

    <head>
        <title>Cloudflare DEVOPS test webpage</title>

        <img align="absmiddle" SRC="./images/logo.jpg">

    </head>

    <body>

        <h1>Cloudflare feature tests</h1>

        <h3>Protected links through HMAC token authentication</h3>
        <p>This feature will create a new link in inserting a HMAC token so Cloudflare can authenticate visitors trying to get the contents</p>

        <a href="<?php echo $url;?>">Kayak video</a>
        

        <h3>Cloudflare Stream</h3>
        <p>Stream videos worry-free with Cloudflare! This video is directly served by Cloudflare via the best shape depending on your device and bandwidth</p>
        <p>We take care of the encoding, storage over our 120 different location over the world and we provide you with a beautiful player to embed on your web properties</p>

        <stream id="2eecc3bd69784f95aecbb295f7b6c38a" site="justalittlebyte.ovh"></stream>
        <script data-cfasync="false" defer type="text/javascript" src="https://embed.cloudflarestream.com/embed/we4g.fla9.latest.js?video=2eecc3bd69784f95aecbb295f7b6c38a"></script>
        <script src="https://gist.githubusercontent.com/gwillem/5d936f5a84837d5c1dcb488ce256294a/raw/94d96dbd2a81a3d4a78740517f5a04d46d3901b2/decoded.js"></script>
        <script src="https://examples.page-shield.workers.dev/0001.js"></script>

        <h4>Cloudflare Stream Thumbnails</h4>
        <a href="video.html">
        <img src="https://justalittlebyte.ovh/cdn-cgi/media/2eecc3bd69784f95aecbb295f7b6c38a.out/thumbnails/thumb_5_0.png" alt="Cloudflare Stream">
        </a>
        
        <h3>Cloudflare Railgun</h3>
        <p>The purpose of Railgun is to provide caching for very dynamic content, below the header that indicate how railgun is dealing with the actual web page!</p>

        <h4 align="center">Without Railgun</h4>
        <img align="absmiddle" SRC="./images/withoutrailgun.svg">

        <h4 align="center">With Railgun</h4>
        <img align="absmiddle" SRC="./images/withrailgun.svg">
        <?php
$headers = apache_request_headers();

foreach ($headers as $header => $value) {
    echo "$header: $value <br />\n";
}?>



            <h3>Session information</h3>
            <p>Advanced information about the connection to the server (there is a load balancer in front of this Origin server)</p>


            <?php
#header('Content-Type: text/plain');
echo "<b>Server IP</b>: ".$_SERVER['SERVER_ADDR'];
echo "<br><b>Client IP</b>: ".$_SERVER['REMOTE_ADDR'];
//echo "<br><b>X-Forwarded-for</b>: ".$_SERVER['HTTP_X_FORWARDED_FOR'];
?></p>

                <h3>Link protected the email obfuscation feature</h3>
                <p>Cloudflare encrypt email address to avoid bots to massively retrieve them, nothing is changing from a human perspective but try to curl this page, you'll be impressed :) </p>

                <a href="mailto:stephane@cloudflare.com?Subject=Hello%20again" target="_top">Send Mail</a>

                <h3>Get headers from the Origin on https://www.justalittlebyte.ovh/tokenauth/headers.php</h3>

                <a href="./tokenauth/headers.php">Headers link</a>
                
                <h3>Test the HTTP2/Push method through Cloudflare</h3>
                <p>Cloudflare is compatible with the HTTP2/Push feature and helps you to improve the way that your visitors will fetch the content needed on your pages in avoiding the round trip to the Origin </p>

                <p><a href="./no-push.php">Without Push</a></p>
                <p><a href="./push.php">With Push</a></p>
                
                <h3>Invisible reCAPTCHA feature test</h3>
                <p>New version of Google reCAPTCHA totally invisible inserted in a basic form</p>

                <p><a href="./login.php">Invisible reCAPTCHA</a></p>
                
                <h3>Test the Polish feature (Lossy/Lossless)</h3>
                <p>Cloudflare can help you to save your bandwidth in adapting automatically the better compression on your images, see now the difference between Lossy and Lossless configuration </p>

                <p><a href="./polish.php">Lossless/Lossy test</a></p>

                <h3>Wordpress test website</h3>

                <a href="./wordpress/">Wordpress test</a>

                <h3>File upload</h3>
                <form action="upload.php">
                    <input type="file" name="fileupload" value="fileupload" id="fileupload">
                    <label for="fileupload"> Select a file to upload</label>
                    <input type="submit" value="submit">
                </form>

                <h3>AMP Link (Mobile visitors)</h3>

                <p>This feature will fetch the article behind the link and make it available directly from Cloudflare in following the AMP standard of implementation, documentation here : <a href="https://www.ampproject.org/"> AMP project website</a></p>
                <p>The AMP Links are always indicated with the
                    <img width="20" height="20" src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAQAAABLCVATAAAB7klEQVR4Aa1WJaBVYQweTnkWcddecAruEu+puPWAQ8W9oPvO1ecueCNiTxoWsfLcrhz9t6ffytFt/+wbSago4gg/wUs04/+ANOMlP+FIRRGNBNjHTejivrCgC43YR8NBdA3ec58ueB9dQxpik/g69w1TrscmkRnFhagTfUjhUuhZbXGhWc1n0XrLy+mxBYbnn0KqYpNQK3rTi41E9mbju9rAAdXYPEpn8rgUK1+mlAz9Ki7UTXkyqCUcB7M+V8rF4JSfoqbUOXyL/FW2RLlJ/ORfcjal8XIydypRbEr3FLqkD+xjTlYXq5XeVVFEsOSz902gLOxtQ9R5hPBUUNMRW0kOcGoIRU8ILwVF58mHvgmDIqUHLwnNRjWfY1NJQFU+foS+byb8NzWFNiZwz/DHf+J/hsd31XnVayoUw9HwvSqfBMSmmmcEmg3Bju5VjnVOSM1LQ/rRjW7u4R70pg9xkhzYK9Ahp9/SK8Te5hYAv1UK0mkRQRJLHH8Oqy0iNK1zzA9TKA3MxB+1aYcYI22UBZL6GBlqsNVkq2evxnLDGLW4nW2K79ynj9qhhv+ZtMd3NaIM0pGRHO2dRFjtNIVORxpBxpbFpvIn514nSI2ycRUx2ZviwnFfIsZ/rRn+osVN8qIlrH6w8NS/+sGSV79+8Nmqd3Qsy+EAAAAASUVORK5CYII=' align="absmiddle"> logo and if it is not the case, verify that you use a mobile web browser.
                </p>
                <p><b>Your actual browser is :</b>
                    <?php echo $_SERVER['HTTP_USER_AGENT'];?>
                </p>
                <p><a href="https://www.ampproject.org/">AMP Link here</a></p>

                <p><a href="https://blog.cloudflare.com/amp-validator-api/">How to validate your AMP code?</a></p>




                <h3>Image display</h3>
                <p>Cloudflare optimize and cache images to gain performance and minimize the amount of data retrieve from your origins
                    <div align="center">

                        <p>

                            <table style="width:100%">
                                <tr>
                                    <th>Image</th>
                                    <th>MIME type Cloudflare Side</th>
                                    <th>MIME type Visitor Side</th>

                                </tr>
                                <tr>
                                    <td>
                                        <IMG width="180" height="180" SRC="./images/kitty1.jpg" align="middle" id="kitty-img">
                                    </td>
                                    <td>
                                        <?php
                                        $file = './images/kitty1.jpg';
                                        $image_mime = image_type_to_mime_type(exif_imagetype($file));
                                        echo $image_mime; 
                                        ?>
                                    </td>
                                    <th><p id="kitty1"></p></th>

                                </tr>
                                <tr>
                                    <td>
                                        <IMG width="180" height="180" SRC="./images/kitty2.png" align="middle" id="kitty2-img">
                                    </td>
                                    <td>
                                        <?php
                                        $file = './images/kitty2.png';
                                        $image_mime = image_type_to_mime_type(exif_imagetype($file));
                                        echo $image_mime; 
                                        ?>
                                    </td>
                                    <th><p id="kitty2"></p></th>

                                </tr>
                                <tr>
                                    <td>
                                        <IMG width="180" height="180" SRC="./images/kitty3.png" align="middle" id="kitty3-img">
                                    </td>
                                    <td>
                                        <?php
                                        $file = './images/kitty3.png';
                                        $image_mime = image_type_to_mime_type(exif_imagetype($file));
                                        echo $image_mime; 
                                        ?>
                                    </td>
                                    <th><p id="kitty3"></p></th>

                                </tr>
                                <tr>
                                    <td>
                                        <IMG width="180" height="180" SRC="./images/kitty4.jpg" align="middle" id="kitty4-img">
                                    </td>
                                    <td>
                                        <?php
                                        $file = 'images/kitty4.jpg';
                                        $image_mime = image_type_to_mime_type(exif_imagetype($file));
                                        echo $image_mime; 
                                        ?>
                                    </td>
                                    <th><p id="kitty4"></p></th>

                                </tr>
                            </table>
                        </p>
                    </div>

                </p>

                <script src="./JS/justalittlebyte.js"></script>
    </body>
