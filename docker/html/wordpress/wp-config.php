<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'justalittlebyte');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'J@r0d!!!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pg7.m6@;GER?c-mrkI[C-}~2WZN&qP2hBIYt(Rdlh,TO85_S`1S,dsBz}^r^Z~h8');
define('SECURE_AUTH_KEY',  'Vs9ux>]`5/4$>L-*>0NZAZAiADNkkL<9B7:D{G^7U|$@Rlr#a@2m/ IGOR(3nEq8');
define('LOGGED_IN_KEY',    '#H<XJL,3wGFC5r*BJyjt0i|wknNU$D$KJ~gKDwk~tBaIMF.h:ymce#2ehNG5<pDC');
define('NONCE_KEY',        '?&{JGBlm:Q(>$/iiubKe2TrZu<K^Qf1@b<ob65@(XtMi%|0sJ_%5J_^HlQ>G@{Cc');
define('AUTH_SALT',        '$6`~H%eC@.<l}/0#l=/e=M{JB~C7#c0r3YID4WOWfl[)MiRvD8(mYmZA:;#qE)Wg');
define('SECURE_AUTH_SALT', 'DYrCGI9t/i5+gHC7^ .M<cE*J$eYXYq0M7[C~disx*sAq`|W$,l5(-Lc.P{.A&`H');
define('LOGGED_IN_SALT',   'aaP@6CyniPeM{Qyj7Z)LS5jhk|M%py7=D-><$>hW h9M93$u,NC`0ckI:ED4~Z*<');
define('NONCE_SALT',       'Q^.LL,uiqvBNS8LN;0SQ~_bctqSV#C:nrq/R(V=jQ+{GN?,280y,35VqK@bsd&Xb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_justalittlebyte';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

