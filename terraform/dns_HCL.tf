variable "cloudflare_email" {}
variable "cloudflare_token" {}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "cloudflare_record" "ec1676f43fa5863e781df04b1c54331R" {
  "type"     = "A"
  "name"     = "c1.justalittlebyte.ovh"
  "value"    = "35.240.91.119"
  "priority" = 0
  "proxied"  = true
  "ttl"      = 1
  "domain"   = "justalittlebyte.ovh"
}

resource "cloudflare_record" "ec1676f43fa5863e781df04b1c543grgrg" {
  "type"     = "A"
  "name"     = "c1.justalittlebyte.ovh"
  "value"    = "35.240.91.120"
  "priority" = 0
  "proxied"  = true
  "ttl"      = 1
  "domain"   = "justalittlebyte.ovh"
}
