variable "cloudflare_email" {}
variable "cloudflare_token" {}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "cloudflare_worker_script" "hmac" {
  zone = "justalittlebyte.ovh"
  content = "${file("hmacValidation.js")}"
}
resource "cloudflare_worker_route" "tokenauth" {
  zone = "justalittlebyte.ovh"
  pattern = "demo.justalittlebyte.ovh/tokenauth/*"
  enabled = true
  depends_on = ["cloudflare_worker_script.hmac"]
}

