# www-test 

A repo that brings up a small HTTP server to test Cloudflare features and provides example of devOPS configuration that could be used alongside with Cloudflare. 

## Features

1. Docker machine
2. Kubernetes configuration
3. Terraform example
4. CI/CD configuration example

## Docker machine

To build the machine (from the root level of this directoty): `docker build www-test ./docker`